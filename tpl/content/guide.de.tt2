[% META
   title   = 'A Guide to Anonymous Boards'
%]

	<header class="title">What is an anonymous board?</header>
	<div class="info">
		<p>The concept of the anonymous board was born on the Japanese site 2channel in 1999. The essential idea is that every poster is completely anonymous. The board may be strictly text only or image and text (commonly referred to as an imageboard). </p>
	</div>

	<header class="title">How is it different from any other bulletin board system or forum?</header>
	<div class="info">
		<p>This means the site has no registration and no barriers, anyone can post at any time instantly; in addition to this, anyone may view any part of the site freely without attaching themselves to a specific name. Beyond the ease of user, users can no longer be judged by their name and instead can only be judged by their individual comments. Hiroyuki, the creator of 2channel, said this in regard to anonymous posting: </p>
		<blockquote>
		If there is a user ID attached to a user, a discussion tends to become a criticizing game. On the other hand, under the anonymous system, even though your opinion/information is criticized, you don't know with whom to be upset. Also with a user ID, those who participate in the site for a long time tend to have authority, and it becomes difficult for a user to disagree with them. Under a perfectly anonymous system, "it's boring", you can say, if it is actually boring. All information is treated equally; only an accurate argument will work.
		</blockquote>
		<p>Another reason to begin posting on anonymous boards is the unique community and sense of humor all boards seem to have. The ability to post anonymously allows for people to voice what they really feel or make comments/jokes about things that are normally taboo. They also develop numerous inside jokes (often called memes) which one will find often become more humorous than any normal joke could.</p>
	</div>

	<header class="title">Common Misconceptions</header>
	<div class="info">
		<p>Not all anonymous boards are like 4chan; in fact, 4chan originally wasn't even like it is now. The "anonymous" identity currently present in 4chan's /b/ is not at all what Anonymous is: a blank name used to represent a user on some boards (see Common Terms for other commonly used names). However, an influx of new users to 4chan's /b/ has left it completely confused and misguided. 4chan's "anonymous" identity that appears at Scientology protests and holds raids on other sites is the exact opposite to what real anonymous posters are like. Please do not treat 4chan's /b/ as an example of what anonymous boards are like! </p>
	</div>

	<header class="title">How to be a successful poster on an anonymous board:</header>
	<div class="info">
		<p>Forget everything you learned posting on a regular forum/board. This is absolutely vital. Anonymous boards are NOT like the rest of the internet. You must come onto an anonymous board with an open mind and a willingness to spend time reading posts BEFORE posting yourself. There is an expectation that you will conform to and learn about the board's culture. A common example is the use of emoticons; many users are used to making emoticons such as ^_^ or =) but these are generally frowned upon and are usually seen as signs of immaturity. You can learn all of this and more by reading the air of the board. Also remember that ANYTHING can be posted, so it is highly likely that you will be offended in some way. After visiting anonymous boards for a while you will begin to be less offended and see why it is silly to be offended in the first place.</p>
	</div>

	<header class="title">Lurking</header>
	<div class="info">
		<p>"Lurking" is browsing a site without posting. This is the most important part of becoming a successful poster. Each and every board has its own unique culture (generally rooted in 2channel one way or another). It is your job to learn it BEFORE making any posts. This process may seem daunting as boards do not have a set of defined parameters for posting. The only way to learn what sort of posts will be enjoyed is to spend time reading other posts. You will slowly learn all of the injokes, common posting style and sense of humor of the board as you read more and more posts. Do not make a post asking about the meaning behind an injoke or other questions about the board unless you absolutely have to as most users will be annoyed by this and simply tell you to lurk more. In addition to this, DO NOT make an introductory post or anything of the sort you should simply become a fellow poster overtime. Obviously this also means that, while posting as Anonymous, your work and your posts might never be directly attributed to you, regardless of the effort you put in them. The time needed for lurking varies: sometimes it takes only a week and other times it takes a few months. You'll know you're ready when you're understanding posts and finding the humor in them. Not all boards revolve around humor but there is always lurking to done just to make sure you aren't posting in a style that the others find annoying.</p>
	</div>


	<header class="title">Where do I register?</header>
	<div class="info">
		<p>Before you begin to type, ask yourself if others will enjoy your post. After you understand the board and how to post, let your creativity run wild! Make witty, insightful, and enjoyable posts! Do not make posts just to boost your ego. This is an ANONYMOUS board. The point is to be unidentifiable so others can focus on your interesting and humorous story about your girlfriend and not relate posts to your name. Don't make bland posts like "Hi I have a girlfriend and we have sex all the time."</p>
	</div>

	<header class="title">Anonymity, Names, IDs and Tripcodes</header>
	<div class="info">
		<p>Many anonymous boards provide a "name" field. Anonymous boards are not just about the software allowing users to post anonymously, it is a cultural thing: on an anonymous board, you are simply expected not to fill in the field if your identity is not relevant to the discussion. Some sites use anonymous board software, but regulars always post under a nickname. Sometimes, as a result, board administrators disable the name field.</p>
		<p>Where the line is drawn between "anonymous boards" and "boards where you may post anonymously" is sometimes a little fuzzy, but this document is only about boards where the overwhelming majority of posts are made anonymously.</p>
		<p>In anonymous boards, names are used only when the identity of the speaker is relevant to the discussion. It doesn't have to be a nickname: on some sites, you will see people using "OP" as their name, to indicate that they are the Original Poster of the thread.</p>
		<p>To avoid impersonation when this risk matters, a tripcode system is frequently provided by the site: add a '#' followed by up to 8 characters at the end of the name field, and the part after the '#' will be displayed in a crypted form. For example, typing "IMPORTANTGUY#tripcode" in the name field will show up as "IMPORTANTGUY!3GqYIJ3Obs" in your posts. You don't have to use a name, a tripcode can be used alone. ("#tripcode" ➔ "!3GqYIJ3Obs")</p>
		<p>It is important to note that on anonymous image boards, many users may treat you with hostility if you use a tripcode because they feel that it is not in the spirit of anonymity. Tripcode users are frequently referred to as "tripfags". This is far less common on text-only boards, where tripcodes are uncommon, but not generally seen as a thing to look down upon.</p>
		<p>Moderators and administrators often use tripcodes when discussing site issues, but in addition of tripcodes their name may be displayed in a different color from regular posters, so that users do not have to verify the tripcode to know that the message comes from the people in charge of the site.</p>
	</div>

	<header class="title">IDs</header>
	<div class="info">
		<p>Most boards will display an ID next to your name. The ID is simpy an encrypted version of your IP address (An address identifying your computer on the internet). Sometimes, depending on a boards configuration, the board name or the thread number are factored into this encryption, so IDs will change between boards and threads. The basic idea behing the ID mechanism is to have a rough idea who is who inside a thread. If you want to hide your ID, enter - again depending on board configuration, it is best to just try this out - either "sage" or just anything at all in the E-Mail / Link field. </p>
	</div>

	<header class="title">sage</header>
	<div class="info">
		<p>Sometimes, you might want to post something to a thread which, in your opinion, is not so interesting that it warrants bumping the thread to the top of the thread list. In that case, you can enter "sage" in the E-Mail / Link field. This will let you post to a thread without bumping it. Another effect of putting "sage" in the Link field is that on most boards, your ID will be replaced by the word "Heaven".</p>
		<p>Note that on 2channel-style boards, unlike on 4chan and similiar websites, "sage-ing" is not generally seen as expressing displeasure with a thread's contents - it's just a mechanism for posting without a bump. Posts like "SAGE THIS SHIT" are generally frowned upon.</p>
	</div>

	<header class="title">Have fun!</header>
	<div class="info">
		<p>Most importantly, have fun and ENJOY your board. I think everyone will find that once you are a part of an anonymous board it will be very difficult to go back to the regular internet as there is not a community quite like this anywhere else. You won't regret it!</p>
	</div>