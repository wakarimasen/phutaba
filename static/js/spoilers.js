function update_file_label(fileinput, max) {
	// find a <span> next to the file input
	var el = fileinput.nextSibling;
	var found = false;
	var span;

	while (el && !found) {
		if (el.nodeName == "SPAN") {
			found = true;
			span = el;
		}
		el = el.nextSibling;
	}

	// add a new <span> to the dom if none was found
	if (!found) {
		var spacer = document.createTextNode("\n ");
		span = document.createElement("span");
		fileinput.parentNode.appendChild(spacer);
		fileinput.parentNode.appendChild(span);
	}

	// put file name(s) into span
	var filename = fileinput.value;

	if (filename.length == 0) {
		span.innerHTML = '';
		return;
	}

	var display_file = format_filename(filename);

	if (typeof fileinput.files == "object" && fileinput.files.length > 1) {
		for (var i = 1, l = fileinput.files.length; i < l; i++) {
			display_file += ' <br />\n&nbsp; '
				+ format_filename(fileinput.files[i].name);
		}
	}

	span.innerHTML = ' <a class="hide" href="javascript:void(0)"'
		+ ' onclick="del_file_input(this,' + max + ')">'
		+ '<img src="/img/icons/cancel.png" width="16" height="16" title="'
		+ msg_remove_file + '" /></a> '
		+ display_file
		+ '<input type="hidden" name="spoilered" value="0" /></input>'
		+ '<input type="checkbox" name="spoilered" value="1" />Spoiler ?</input>' + '\n';
}
