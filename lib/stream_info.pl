use strict;
use LWP::Simple;
use JSON;

sub form_radio_status {
    my $result_string = "";
    my $stream_json = get(RADIO_SERVER . RADIO_STATUS_JSON);
    my $stream_info;
    if (defined $stream_json) {
	$stream_info = decode_json $stream_json;
	$stream_info -> {'success'} =  
		scalar @{$stream_info -> {'stream'}};
    } else {
	$stream_info -> {'success'} = -1;
    }

    if (${$stream_info}{'success'} > 0) {
	$result_string .= "<p>" . NOW_AT_RADIO . "</p>";
    } elsif ($stream_info -> {'success'} == -1) {
	$result_string .= "<p>Radio is not responding</p>";
    }

    foreach my $channel (@{$stream_info -> {'stream'}}) {
	my $play = $channel -> {'play'};
	$result_string .= "<p><br />";
	$result_string .= $channel -> {'description'};
	$result_string .= (TRANSMISSION_BY . $channel -> {'name'})
	     if ($channel -> {'name'});
	$result_string .= "</p>\n";

	if ($play -> {'URL'}) {
	    $result_string .= "<p>";
	    $result_string .= "<a href=\"" . RADIO_SERVER;
	    $result_string .= "/" . $play -> {'URL'};
	    $result_string .= "\">Online player</a></p>";
	}

	$result_string .= "<p>";

	if ($play -> {'M3U'}) {
	    $result_string .= "<a href=\"" . RADIO_SERVER;
	    $result_string .= $play -> {'M3U'};
	    $result_string .= "\">M3U</a>";
	}

	if (   ($play -> {'M3U'})
	    && ($play -> {'XSPF'}) ) {
	    $result_string .= " - ";
	}

	if ($play -> {'XSPF'}) {
	    $result_string .= "<a href=\"" . RADIO_SERVER;
	    $result_string .= $play -> {'XSPF'};
	    $result_string .= "\">XSPF</a>";
	}

	$result_string .= "</p>";
    }

    return $result_string;
};

1;